# Credit card service (Consumer side)

## Getting started

This is an example with a consumer service with spring cloud contract library integration.

### Main code part
With POST endpoint `/credit-card-application` wich includes CreditCardRequest request object.
This endpoint consumes `/credit-scores` endpoint checking user access.

### Testing part
In **CreditcardserviceApplicationTests** integration test class with `MockMvc`
It is configured with `com.retailbank:creditcardcheckservice+` stub in port 8080.
`/credit-card-application` request has uuid and actual Date generated in bean creation. **UUID** should be the same as `/credit-scores` response, so in CreditcardserviceApplicationTests it is checked.


### Running
We need install stubs provider project, `com.retailbank:creditcardcheckservice+`
It is using maven wrapper so in initialization:
```sh
./mvnw clean install
```