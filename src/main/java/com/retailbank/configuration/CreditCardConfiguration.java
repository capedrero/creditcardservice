package com.retailbank.configuration;

import com.retailbank.service.CreditCardService;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.client.RestTemplate;

@Configuration
public class CreditCardConfiguration{

  @Bean
  public RestTemplate restTemplate(){
    return new RestTemplate();
  }

}
