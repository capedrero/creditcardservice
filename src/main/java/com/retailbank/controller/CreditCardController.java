package com.retailbank.controller;

import com.retailbank.model.CreditCardRequest;
import com.retailbank.model.CreditCardStatusResponse;
import com.retailbank.service.CreditCardService;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class CreditCardController {


  private CreditCardService creditCardService;


  public CreditCardController(CreditCardService creditCardService) {
    this.creditCardService = creditCardService;
  }

  @PostMapping(value = "/credit-card-application", consumes = MediaType.APPLICATION_JSON_VALUE)
  public CreditCardStatusResponse creditCardApplication(@RequestBody CreditCardRequest creditCardRequest){
    return creditCardService.getStatusCreditCard(creditCardRequest);
  }

}
