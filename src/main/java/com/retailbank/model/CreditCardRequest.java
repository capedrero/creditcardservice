package com.retailbank.model;

import java.util.Objects;

public class CreditCardRequest {
private Integer number;
  private CardType type;

  public CreditCardRequest() {
  }

  public CreditCardRequest(Integer number, CardType type) {
    this.number = number;
    this.type = type;
  }

  public Integer getNumber() {
    return number;
  }

  public void setNumber(Integer number) {
    this.number = number;
  }

  public CardType getType() {
    return type;
  }

  public void setType(CardType type) {
    this.type = type;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (!(o instanceof CreditCardRequest)) {
      return false;
    }
    CreditCardRequest that = (CreditCardRequest) o;
    return Objects.equals(number, that.number) &&
        type == that.type;
  }

  @Override
  public int hashCode() {
    return Objects.hash(number, type);
  }

  public enum CardType {
    GOLD;
  }
}
