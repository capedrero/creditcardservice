package com.retailbank.model;

import java.time.LocalDate;
import java.util.Objects;
import java.util.UUID;

public class CreditCardCheckRequest {

  private int citizenNumber;
  private String requestDate = LocalDate.now().toString();
  private UUID uuid = UUID.randomUUID();

  public CreditCardCheckRequest() {
  }

  public CreditCardCheckRequest(int citizenNumber) {
    this.citizenNumber = citizenNumber;
  }

  public int getCitizenNumber() {
    return citizenNumber;
  }

  public String getRequestDate() {
    return requestDate;
  }

  public UUID getUuid() {
    return uuid;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (!(o instanceof CreditCardCheckRequest)) {
      return false;
    }
    CreditCardCheckRequest that = (CreditCardCheckRequest) o;
    return citizenNumber == that.citizenNumber &&
        Objects.equals(requestDate, that.requestDate) &&
        Objects.equals(uuid, that.uuid);
  }

  @Override
  public int hashCode() {
    return Objects.hash(citizenNumber, requestDate, uuid);
  }
}
