package com.retailbank.model;

import java.util.Objects;
import java.util.UUID;

public class CreditCardStatusResponse {

  private Status status;
  private UUID uuid;

  public CreditCardStatusResponse() {
  }

  public CreditCardStatusResponse(Status status, UUID uuid) {
    this.status = status;
    this.uuid = uuid;
  }

  public Status getStatus() {
    return status;
  }

  public void setStatus(Status status) {
    this.status = status;
  }

  public UUID getUuid() {
    return uuid;
  }

  public void setUuid(UUID uuid) {
    this.uuid = uuid;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (!(o instanceof CreditCardStatusResponse)) {
      return false;
    }
    CreditCardStatusResponse that = (CreditCardStatusResponse) o;
    return status == that.status &&
        Objects.equals(uuid, that.uuid);
  }

  @Override
  public int hashCode() {
    return Objects.hash(status, uuid);
  }

  public enum Status {
    GRANTED, DENIED
  }
}
