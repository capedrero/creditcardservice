package com.retailbank.service;

import com.retailbank.model.CreditCardCheckRequest;
import com.retailbank.model.CreditCardRequest;
import com.retailbank.model.CreditCardRequest.CardType;
import com.retailbank.model.CreditCardStatusResponse;
import com.retailbank.model.CreditCardStatusResponse.Status;
import com.retailbank.model.CreditcardCheckResponse;
import com.retailbank.model.CreditcardCheckResponse.Score;
import java.util.UUID;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

@Component
public class CreditCardService {

  private RestTemplate restTemplate;

  public CreditCardService(RestTemplate restTemplate) {
    this.restTemplate = restTemplate;
  }

  public CreditCardStatusResponse getStatusCreditCard(CreditCardRequest creditCardRequest) {

    final CreditCardCheckRequest creditCardCheckRequest = new CreditCardCheckRequest(
        creditCardRequest.getNumber());
    final CreditcardCheckResponse creditcardCheckResponse = restTemplate
        .postForObject("http://localhost:8080/credit-scores", creditCardCheckRequest,
            CreditcardCheckResponse.class);

    if (!creditCardCheckRequest.getUuid().equals(creditcardCheckResponse.getUuid())) {
      throw new RuntimeException("Request uuid is not the same to Creditcardcheckservice response");
    }

    final Score score = creditcardCheckResponse.getScore();
    final UUID uuid = creditcardCheckResponse.getUuid();
    if (Score.HIGH == score) {
      if (CardType.GOLD == creditCardRequest.getType()) {
        return new CreditCardStatusResponse(Status.GRANTED, uuid);
      }
    } else if (Score.LOW == score) {
      return new CreditCardStatusResponse(Status.DENIED, uuid);
    }
    throw new RuntimeException("Not implemented yet");
  }
}
