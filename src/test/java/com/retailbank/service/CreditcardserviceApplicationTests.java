package com.retailbank.service;


import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.retailbank.model.CreditCardRequest;
import com.retailbank.model.CreditCardRequest.CardType;
import org.hamcrest.Matchers;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.cloud.contract.stubrunner.spring.AutoConfigureStubRunner;
import org.springframework.cloud.contract.stubrunner.spring.StubRunnerProperties.StubsMode;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;


@SpringBootTest
@AutoConfigureMockMvc
@AutoConfigureStubRunner(
    stubsMode = StubsMode.LOCAL, ids = "com.retailbank:creditcardcheckservice:+:stubs:8080")
class CreditcardserviceApplicationTests {

  @Autowired
  private WebApplicationContext wac;
  @Autowired
  private MockMvc mockMvc;

  @BeforeEach
  public void setUp() {
    this.mockMvc = MockMvcBuilders.webAppContextSetup(wac).build();
  }

  private ObjectMapper objectMapper = new ObjectMapper();


  @Test
  void should_grant_application_when_score_is_high() throws Exception {
    this.mockMvc.perform(MockMvcRequestBuilders.post("/credit-card-application").contentType(
        MediaType.APPLICATION_JSON).content(
        (objectMapper.writeValueAsString(new CreditCardRequest(1234, CardType.GOLD)))
    )).andDo(MockMvcResultHandlers.print())
        .andExpect(MockMvcResultMatchers.status().isOk())
        .andExpect(jsonPath("$.uuid").value(Matchers.notNullValue()))
        .andExpect(jsonPath("$.status").value("GRANTED"))
        .andExpect(
            MockMvcResultMatchers.content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON));


  }

  @Test
  void should_deny_application_when_score_is_low() throws Exception {
    this.mockMvc.perform(MockMvcRequestBuilders.post("/credit-card-application").contentType(
        MediaType.APPLICATION_JSON).content(
        (objectMapper.writeValueAsString(new CreditCardRequest(4444, CardType.GOLD)))
    )).andDo(MockMvcResultHandlers.print())
        .andExpect(MockMvcResultMatchers.status().isOk())
        .andExpect(jsonPath("$.uuid").value(Matchers.notNullValue()))
        .andExpect(jsonPath("$.status").value("DENIED"))
        .andExpect(
            MockMvcResultMatchers.content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON));
  }

}
